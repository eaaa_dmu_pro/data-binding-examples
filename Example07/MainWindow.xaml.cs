﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace ColorSlider
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = slColorR;
            Binding backgroundColorBinding = new Binding();
            backgroundColorBinding.Converter = new ColorConverter();
            backgroundColorBinding.Mode = BindingMode.OneWay;
            backgroundColorBinding.Path = new PropertyPath(nameof(slColorR.Value));
            this.SetBinding(MainWindow.BackgroundProperty, backgroundColorBinding);
        }

        private void ColorSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Color color = Color.FromRgb((byte)slColorR.Value, (byte)slColorG.Value, (byte)slColorB.Value);
            this.Background = new SolidColorBrush(color);
        }

        class ColorConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                double? redValue = value as double?;

                Color color = Color.FromRgb((byte)redValue, 0, 0);
                return new SolidColorBrush(color);
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }
    }
}
