﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace WpfObservableCollection
{
    public class Person    {

        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        int birthYear;
        public int BirthYear
        {
            get { return birthYear; }
            set { birthYear = value; }
        }
    }




}
