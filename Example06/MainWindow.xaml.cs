﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace WpfBindingConverters
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Car car = null;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            car= new Car { Model="VW Passat", ProdYear=2010, IsRegistered=true, RegNr="EH20345"};
            TopStackPanel.DataContext = car;

            Binding b=new Binding();
            //b.Source = car;
            b.Converter=new BooleanToVisibilityConverter();
            b.Mode=BindingMode.OneWay;
            b.Path=new PropertyPath("IsRegistered");

            tBoxRegNr.SetBinding(TextBox.VisibilityProperty, b);
        }

        private void btnShowCar_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(car.ToString());
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {
            car.ProdYear++;
        }

        private void btnMinus_Click(object sender, RoutedEventArgs e)
        {
            car.ProdYear--;
        }
    }
}
